<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('ui.frontend.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::view('/contact-us', 'frontend.contact');
Route::view('/news/{story}', 'frontend.list');
Route::view('/single-post', 'frontend.single');
Route::view('/about-us', 'frontend.about');
