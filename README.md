# News Mania Demo

## Set Instructions

1. Clone repository `git clone https://tridibdawn0@bitbucket.org/tridibdawn0/news-mania-demo.git`.

2. Run `composer install`.

3. Copy __.env.example__ and create __.env__ file and paste the content.

4. Setup `database` configuration.

5. Run `php artisan key:generate`.

6. Run `npm install && npm run dev`.

7. Run `php artisan migrate`.

8. Run `php artisan serve`.

9. Visit `127.0.0.1:8000`.
