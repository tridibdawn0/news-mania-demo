    
    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

      <!-- Top Header Area -->
      <div class="top-header-area">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <div class="top-header-content d-flex align-items-center justify-content-between">
                          <!-- Logo -->
                          <div class="logo">
                              <a href="{{ url('/') }}"><img src="{{ asset('images/news-mania-demo-logo.svg') }}" alt=""></a>
                          </div>

                          <!-- Login Search Area -->
                          <div class="login-search-area d-flex align-items-center">
                              <!-- Login -->
                              @guest
                              <div class="login d-flex">
                                  <a href="{{ url('/login') }}">Login</a>
                                  <a href="{{ url('/register') }}">Register</a>
                              </div>
                              @else 
                              <div class="login d-flex">
                                <li class="nav-item dropdown" style="list-style: none">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                      Welcome,  {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
    
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"
                                            style="color: #AAAAAA">
                                            {{ __('Logout') }}
                                        </a>
    
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                              </div>
                              @endguest
                              <!-- Search Form -->
                              <div class="search-form">
                                  <form action="#" method="post">
                                      <input type="search" name="search" class="form-control" placeholder="Search">
                                      <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <!-- Navbar Area -->
      <div class="newspaper-main-menu" id="stickyMenu">
          <div class="classy-nav-container breakpoint-off">
              <div class="container">
                  <!-- Menu -->
                  <nav class="classy-navbar justify-content-between" id="newspaperNav">

                      <!-- Logo -->
                      <div class="logo">
                          <a href="{{ url('/') }}"><img src="{{ asset('images/news-mania-demo-logo-3.svg') }}" alt=""></a>
                      </div>

                      <!-- Navbar Toggler -->
                      <div class="classy-navbar-toggler">
                          <span class="navbarToggler"><span></span><span></span><span></span></span>
                      </div>

                      <!-- Menu -->
                      <div class="classy-menu">

                          <!-- close btn -->
                          <div class="classycloseIcon">
                              <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                          </div>

                          <!-- Nav Start -->
                          <div class="classynav">
                              <ul>
                                  <li class="active"><a href="{{ url('/') }}">Home</a></li>
                                  <li><a href="#">Pages</a>
                                      <ul class="dropdown">
                                          <li><a href="{{ url('/') }}">Home</a></li>
                                          <li><a href="{{ url('/news/categories') }}">Catagories</a></li>
                                          <li><a href="{{ url('/single-post') }}">Single Articles</a></li>
                                          <li><a href="{{ url('/about-us') }}">About Us</a></li>
                                          <li><a href="{{ url('/contact-us') }}">Contact</a></li>
                                          <li><a href="#">Dropdown</a>
                                              <ul class="dropdown">
                                                  <li><a href="{{ url('/') }}">Home</a></li>
                                                  <li><a href="{{ url('/news/categories') }}">Catagories</a></li>
                                                  <li><a href="{{ url('/single-post') }}">Single Articles</a></li>
                                                  <li><a href="{{ url('/about-us') }}">About Us</a></li>
                                                  <li><a href="{{ url('/contact-us') }}">Contact</a></li>
                                              </ul>
                                          </li>
                                      </ul>
                                  </li>
                                  <li><a href="#">Mega Menu</a>
                                      <div class="megamenu">
                                          <ul class="single-mega cn-col-4">
                                              <li class="title">Catagories</li>
                                              <li><a href="{{ url('/') }}">Home</a></li>
                                              <li><a href="{{ url('/news/categories') }}">Catagories</a></li>
                                              <li><a href="{{ url('/single-post') }}">Single Articles</a></li>
                                              <li><a href="{{ url('/about-us') }}">About Us</a></li>
                                              <li><a href="{{ url('/contact-us') }}">Contact</a></li>
                                          </ul>
                                          <ul class="single-mega cn-col-4">
                                              <li class="title">Catagories</li>
                                              <li><a href="{{ url('/') }}">Home</a></li>
                                              <li><a href="{{ url('/news/categories') }}">Catagories</a></li>
                                              <li><a href="{{ url('/single-post') }}">Single Articles</a></li>
                                              <li><a href="{{ url('/about-us') }}">About Us</a></li>
                                              <li><a href="{{ url('/contact-us') }}">Contact</a></li>
                                          </ul>
                                          <ul class="single-mega cn-col-4">
                                              <li class="title">Catagories</li>
                                              <li><a href="{{ url('/') }}">Home</a></li>
                                              <li><a href="{{ url('/news/categories') }}">Catagories</a></li>
                                              <li><a href="{{ url('/single-post') }}">Single Articles</a></li>
                                              <li><a href="{{ url('/about-us') }}">About Us</a></li>
                                              <li><a href="{{ url('/contact-us') }}">Contact</a></li>
                                          </ul>
                                          <div class="single-mega cn-col-4">
                                              <!-- Single Featured Post -->
                                              <div class="single-blog-post small-featured-post d-flex">
                                                  <div class="post-thumb">
                                                      <a href="#"><img src="{{ asset('img/bg-img/23.jpg') }}" alt=""></a>
                                                  </div>
                                                  <div class="post-data">
                                                      <a href="{{ url('/news/travel') }}" class="post-catagory">Travel</a>
                                                      <div class="post-meta">
                                                          <a href="#" class="post-title">
                                                              <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                                          </a>
                                                          <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                                      </div>
                                                  </div>
                                              </div>

                                              <!-- Single Featured Post -->
                                              <div class="single-blog-post small-featured-post d-flex">
                                                  <div class="post-thumb">
                                                      <a href="#"><img src="{{ asset('img/bg-img/24.jpg') }}" alt=""></a>
                                                  </div>
                                                  <div class="post-data">
                                                      <a href="{{ url('/news/travel') }}" class="post-catagory">Politics</a>
                                                      <div class="post-meta">
                                                          <a href="#" class="post-title">
                                                              <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                                          </a>
                                                          <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                                  <li><a href="{{ url('/news/travel') }}">Politics</a></li>
                                  <li><a href="{{ url('/news/breaking-news') }}">Breaking News</a></li>
                                  <li><a href="{{ url('/news/business') }}">Business</a></li>
                                  <li><a href="{{ url('/news/technology') }}">Technology</a></li>
                                  <li><a href="{{ url('/news/health') }}">Health</a></li>
                                  <li><a href="{{ url('/news/travel') }}">Travel</a></li>
                                  <li><a href="{{ url('/news/sports') }}">Sports</a></li>
                                  <li><a href="{{ url('/contact-us') }}">Contact</a></li>
                              </ul>
                          </div>
                          <!-- Nav End -->
                      </div>
                  </nav>
              </div>
          </div>
      </div>
  </header>
  <!-- ##### Header Area End ##### -->