@extends('layouts.backend.app')

@section('content')

@include('ui.backend.header')

@include('ui.backend.sidebar')

@yield('wrapper')

@include('ui.backend.footer')

@endsection