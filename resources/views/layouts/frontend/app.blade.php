<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>News Mania | Voice of People</title>
  <!-- Favicon -->
  <link rel="icon" href="{{ asset('images/site-logo.svg') }}">

  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
</head>
<body>
  @include('ui.frontend.header')

  @yield('content')

  @include('ui.frontend.footer')

  <!-- Bottom Footer Area -->
  <div class="bottom-footer-area">
      <div class="container h-100">
          <div class="row h-100 align-items-center">
              <div class="col-12">
                  <!-- Copywrite -->
                  <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <a href="http://newsmania.in" target="_blank">www.newsmania.in</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
              </div>
          </div>
      </div>
  </div>
  </footer>
  <!-- ##### Footer Area Start ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jQuery-2.2.4 js -->
  <script src="{{ asset('js/frontend/jquery/jquery-2.2.4.min.js') }}"></script>
  <!-- Popper js -->
  <script src="{{ asset('js/frontend/bootstrap/popper.min.js') }}"></script>
  <!-- Bootstrap js -->
  <script src="{{ asset('js/frontend/bootstrap/bootstrap.min.js') }}"></script>
  <!-- All Plugins js -->
  <script src="{{ asset('js/frontend/plugins/plugins.js') }}"></script>
  <!-- Active js -->
  <script src="{{ asset('js/frontend/active.js') }}"></script>
</body>
</html>